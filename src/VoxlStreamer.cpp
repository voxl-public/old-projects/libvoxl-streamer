/*******************************************************************************
 * Copyright 2019 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>
#include <math.h>
#include <unistd.h>

#include "VoxlStreamer.hpp"

/**
 * 
 */
int VoxlStreamer::Initialize(char* ip, int port)
{
  if (!_initialized)
  {
    IpServer* server_ptr = new IpServer();
    server_ptr->CreateSocket(ip, port);
    server_ptr->BindSocket();
    server_ptr->ConnectClient();

    Initialize(server_ptr);
  }
  else
  {
    printf("WARN: VoxlStreamer is already initialized.\n");
  }
  return 0;
}

/**
 * 
 */
int VoxlStreamer::Initialize(IpServer* tcp_server)
{
  if (!_initialized)
  {
    _server_ptr = tcp_server;
    _initialized = true;
  }
  else
  {
    printf("WARN: VoxlStreamer is already initialized.\n");
  }
  return 0;
}

/**
 * 
 */
int VoxlStreamer::CheckConnection()
{
  if (_initialized)
  {
    if (_server_ptr->CheckSocket() < 0)
    {
      printf("ERROR: Error with TCP connection.\n");
      return -1;
    }
  }
  return 0;
}

int VoxlStreamer::ReceiveCommand()
{
  // Receive any messages
  return _server_ptr->RecvMessage();
}

/**
 * 
 */
void VoxlStreamer::SendImage(uint8_t* image, const uint16_t width, const uint16_t height,
                             uint8_t opts[4], const uint32_t frame_id, const uint64_t timestamp_ns)
{
  if (image != NULL)
  {
    // Send image
    size_t buffer_size = width * height;
    _server_ptr->SendMessage(image, buffer_size * sizeof(uint8_t), MONOCHROME_IMG,
                             width, height, opts, frame_id, timestamp_ns);
  }

  return;
}

/**
 * 
 */
void VoxlStreamer::SendStereoImage(uint8_t* left_image, uint8_t* right_image,
                                   const uint16_t width, const uint16_t height, uint8_t opts[4],
                                   const uint32_t frame_id, const uint64_t timestamp_ns)
{
  if (left_image != NULL && right_image != NULL)
  {
    size_t buffer_size = width * height;

    // Send left image
    _server_ptr->SendMessage(left_image, buffer_size*sizeof(uint8_t), MONOCHROME_STEREO_IMG_L,
                             width, height, opts, frame_id, timestamp_ns);

    // Send right image
    _server_ptr->SendMessage(right_image, buffer_size*sizeof(uint8_t), MONOCHROME_STEREO_IMG_R,
                             width, height, opts, frame_id, timestamp_ns);
  }

  return;
}

/**
 * 
 */
void VoxlStreamer::SendValue(char* label, double value, uint8_t index,
                             uint8_t opts[4], const uint32_t frame_id, const uint64_t timestamp_ns)
{
  if (label != NULL)
  {
    // Create message buffer
    uint8_t* msg_buffer = new uint8_t[25];
    memset(msg_buffer, 0, sizeof(msg_buffer));

    // Byte 0 - 15: label string
    strncpy((char*)msg_buffer, label, 15);
    msg_buffer[15] = '\0';

    // Byte 16 - 23: double value
    *(double*)(msg_buffer + 16) = value;

    // Byte 24: value index
    msg_buffer[24] = index;

    // Send message buffer
    _server_ptr->SendMessage(msg_buffer, 25*sizeof(uint8_t), VALUE_PAIR,
                             25, 1, &opts[0], frame_id, timestamp_ns);
  }

  return;
}
